
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"
// Lấy giá trị user_id từ LocalStorage
var accessToken = localStorage.getItem('token');
var vData = [];
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onCLick();

    callProperties();
    $("#all-price").on("click", function () {
        $("#row-realestate").html("");
        loadProperties(vData);


    })
    $("#under-1000").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findPropertiesByPrice(1000);
        loadProperties(vDataFind);


    })
    $("#1000-1500").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findPropertiesByPriceBetween(1000, 1500);
        loadProperties(vDataFind);


    })
    $("#1500-2000").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findPropertiesByPriceBetween(1500, 2000);
        loadProperties(vDataFind);


    })
    $("#2000-2500").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findPropertiesByPriceBetween(2000, 2500);
        loadProperties(vDataFind);


    })
    $("#2500-3000").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findPropertiesByPriceBetween(2500, 3000);
        loadProperties(vDataFind);


    })
    $("#3500-4000").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findPropertiesByPriceBetween(3500, 4000);
        loadProperties(vDataFind);


    })
    $("#5000-10000").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findPropertiesByPriceBetween(5000, 10000);
        loadProperties(vDataFind);


    })
    $("#30-50").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findAcreage(30, 50);
        loadProperties(vDataFind);

    })
    $("#50-80").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findAcreage(50, 80);
        loadProperties(vDataFind);

    })
    $("#80-110").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findAcreage(80, 110);
        loadProperties(vDataFind);

    })
    $("#110-140").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findAcreage(110, 140);
        loadProperties(vDataFind);

    })
    $("#140-170").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findAcreage(140, 170);
        loadProperties(vDataFind);

    })
    $("#170-200").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findAcreage(170, 200);
        loadProperties(vDataFind);

    })
    $("#200-300").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findAcreage(200, 300);
        loadProperties(vDataFind);

    })
    $("#300-400").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findAcreage(300, 400);
        loadProperties(vDataFind);

    })
    $("#500-10000").on("click", function () {
        $("#row-realestate").html("");
        var vDataFind = findAcreage(500, 10000);
        loadProperties(vDataFind);

    })
    $("#btn-search").on("click", function () {
        $("#row-realestate").html("");
        var vDataAddress = findAddress();
        loadProperties(vDataAddress);
    })


    onLoadProvince();
    $("#select-province").on("change", function () {
        var vId = $("#select-province").val();
        $("#select-district").html("");
        onLoadDistrict(vId);
    })
    $("#select-district").on("change", function () {
        var vId = $("#select-district").val();
        $("#select-ward").html("");
        onLoadWard(vId);
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function callProperties() {
    $.ajax({
        url: gBASE_URL + "realestate",

        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            console.log("success");
            vData = paramRes;

            loadProperties(paramRes);
        },
        error: function (paramRes) {
            alert(paramRes.status);
        }
    })
}
function callPropertiesFilPrice() {
    $.ajax({
        url: gBASE_URL + "realestate",

        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            console.log("success");

            loadFilterPrice(paramRes);
        },
        error: function (paramRes) {
            alert(paramRes.status);
        }
    })
}

function loadProperties(paramData) {

    for (var bI = 0; bI < paramData.length; bI++) {
        if (paramData[bI].status == 1) {
            $("#row-realestate").append($("<div>", {
                class: "col-sm-4 mt-5",


            })
                .append($("<div>", {
                    class: "card card-properties",
                })
                    .append($("<img>", {
                        class: "card-img-top",
                        src: "./image/" + paramData[bI].photo,
                        // width: "280.5",
                        height: "201.656"

                    }))
                    .append($("<div>", {
                        class: "card-body "

                    })
                        .append($("<p>", {
                            class: "title-properties",
                            html: paramData[bI].title,
                        }))
                        .append($("<p>", {
                            class: "address-properties",
                            html: paramData[bI].address,
                        }))
                        .append($("<div>", {
                            class: "row",
                        })
                            .append($("<div>", {
                                class: "col-sm-4",
                                html: '<img src="./image/area.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].acreage + "m2"

                            }))
                            .append($("<div>", {
                                class: "col-sm-3",
                                html: '<img src="./image/bedroom.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].bedroom

                            }))
                            .append($("<div>", {
                                class: "col-sm-3",
                                html: '<img src="./image/bathtub.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].bath
                            }))
                            .append($("<div>", {
                                class: "col-sm-2",
                                html: '<img src="./image/tile.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].totalFloors
                            }))
                        )
                    )
                    .append($("<div>", {
                        class: "card-footer",
                    })
                        .append($("<div>", {
                            class: "row mt-3",

                        })
                            .append($("<div>", {
                                class: "col-sm-6 price-properties",
                                html: "Price: $" + paramData[bI].price,
                            }))
                            .append($("<div>", {
                                class: "col-sm-6 text-right",
                                html: '<img src="./image/love-hand-drawn-heart-symbol-outline.png" alt="" class="img-icon-properties">'
                            }))
                        )
                    )
                )

            )
        }

    }

}


function findPropertiesByPrice(paramPrice) {
    var vArrayResult = [];

    for (var bI = 0; bI < vData.length; bI++) {
        if (vData[bI].price < paramPrice) {
            vArrayResult.push(vData[bI]);
        }

    }
    return vArrayResult;
}
function findPropertiesByPriceBetween(paramPrice1, paramPrice2) {
    var vArrayResult = [];

    for (var bI = 0; bI < vData.length; bI++) {
        if (vData[bI].price >= paramPrice1 && vData[bI].price <= paramPrice2) {
            vArrayResult.push(vData[bI]);
        }

    }
    return vArrayResult;
}

function findAcreage(paramAc1, paramAc2) {
    var vArrayResult = [];

    for (var bI = 0; bI < vData.length; bI++) {
        if (vData[bI].acreage >= paramAc1 && vData[bI].acreage <= paramAc2) {
            vArrayResult.push(vData[bI]);
        }

    }
    return vArrayResult;
}
function onLoadProvince() {
    $.ajax({
        url: gBASE_URL + "province",
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            handleProvince(paramRes);
            // console.log(accessToken)
            // alert("hello");
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadDistrict(paramId) {
    $.ajax({
        url: gBASE_URL + "province/" + paramId,
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            handleDistrict(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadWard(paramId) {
    $.ajax({
        url: gBASE_URL + "district/" + paramId,
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            handleWard(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onCLick() {
    $("#buy").on("click", function () {
        $("#buy").addClass("clicked");
    })

}
function handleProvince(paramData) {
    for (var bI = 0; bI < paramData.length; bI++) {
        $("#select-province").append($("<option>", {
            text: paramData[bI].name,
            value: paramData[bI].id
        }))
    }

}
function handleDistrict(paramData) {
    for (var bI = 0; bI < paramData.districts.length; bI++) {
        $("#select-district").append($("<option>", {
            text: paramData.districts[bI].name,
            value: paramData.districts[bI].id
        }))
    }

}
function handleWard(paramData) {
    for (var bI = 0; bI < paramData.wards.length; bI++) {
        $("#select-ward").append($("<option>", {
            text: paramData.wards[bI].name,
            value: paramData.wards[bI].id
        }))
    }

}
function findAddress() {
    var vProvince = $('#select-province :selected').text();
    var vDistrict = $('#select-district :selected').text();
    var vWard = $('#select-ward :selected').text();
    var vAddress = vProvince + "-" + vDistrict + "-" + vWard;
    var chuoiTimKiem = vAddress;

    var ketQua = vData.filter(function (item) {
        return item.address.includes(chuoiTimKiem);
    });
    return ketQua;



}





