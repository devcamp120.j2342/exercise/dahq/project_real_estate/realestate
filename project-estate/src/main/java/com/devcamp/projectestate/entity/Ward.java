package com.devcamp.projectestate.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ward")
public class Ward {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "_name")
    private String name;
    @Column(name = "_prefix")
    private String prefix;
    @ManyToOne
    @JoinColumn(name = "_district_id")
    private District district;
    @OneToMany(mappedBy = "ward")
    private List<Project> projects;
    @OneToMany(mappedBy = "ward")
    private List<Realestate> realestates;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    // public District getDistrict() {
    // return district;
    // }

    // public void setDistrict(District district) {
    // this.district = district;
    // }

    public Ward() {
    }

    public List<Realestate> getRealestates() {
        return realestates;
    }

    public void setRealestates(List<Realestate> realestates) {
        this.realestates = realestates;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public Ward(int id, String name, String prefix, District district, List<Project> projects,
            List<Realestate> realestates) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.district = district;
        this.projects = projects;
        this.realestates = realestates;
    }

    // public void setProvince(Province province) {
    // this.province = province;
    // }

}
