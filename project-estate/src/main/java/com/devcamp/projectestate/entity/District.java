package com.devcamp.projectestate.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "district")
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "_name")
    private String name;
    @Column(name = "_prefix")
    private String prefix;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "_province_id")
    private Province province;
    @OneToMany(mappedBy = "district")
    private List<Ward> wards;
    @OneToMany(mappedBy = "district")
    private List<Street> streets;
    @OneToMany(mappedBy = "district")
    private List<Project> projects;
    @OneToMany(mappedBy = "district")
    private List<Realestate> realestates;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public District() {
    }

    // public Province getProvince() {
    // return province;
    // }

    public List<Ward> getWards() {
        return wards;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }

    public void setWards(List<Ward> wards) {
        this.wards = wards;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Realestate> getRealestates() {
        return realestates;
    }

    public void setRealestates(List<Realestate> realestates) {
        this.realestates = realestates;
    }

    public District(int id, String name, String prefix, Province province, List<Ward> wards, List<Street> streets,
            List<Project> projects, List<Realestate> realestates) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
        this.wards = wards;
        this.streets = streets;
        this.projects = projects;
        this.realestates = realestates;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

}
