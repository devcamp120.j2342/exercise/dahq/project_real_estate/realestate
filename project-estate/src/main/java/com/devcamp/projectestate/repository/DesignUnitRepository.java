package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.DesignUnit;

public interface DesignUnitRepository extends JpaRepository<DesignUnit, Integer> {

}
