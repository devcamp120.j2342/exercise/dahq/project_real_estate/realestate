package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.Utilities;

public interface UtilitesRepository extends JpaRepository<Utilities, Integer> {

}
