package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {

}
