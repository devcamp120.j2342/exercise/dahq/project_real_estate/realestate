package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Integer> {

}
