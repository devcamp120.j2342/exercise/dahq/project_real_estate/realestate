package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.constructionContractor;

public interface ConstructionContractorRepository extends JpaRepository<constructionContractor, Integer> {

}
