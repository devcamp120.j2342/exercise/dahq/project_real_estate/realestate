package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.Realestate;

public interface RealestateRepository extends JpaRepository<Realestate, Integer> {

}
