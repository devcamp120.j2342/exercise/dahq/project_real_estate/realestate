package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer> {

}
