package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.Customers;

public interface CustomerRepository extends JpaRepository<Customers, Long> {

}
