package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {

}
