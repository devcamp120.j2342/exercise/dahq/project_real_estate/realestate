package com.devcamp.projectestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.projectestate.entity.Employees;

public interface EmployeeRepository extends JpaRepository<Employees, Integer> {

}
