package com.devcamp.projectestate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.projectestate.entity.Customers;
import com.devcamp.projectestate.entity.DesignUnit;
import com.devcamp.projectestate.repository.DesignUnitRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DesignUnitController {
    @Autowired
    DesignUnitRepository designUnitRepository;

    @GetMapping("/design")
    public List<DesignUnit> getAllDesignUnit() {
        return designUnitRepository.findAll();
    }

    @GetMapping("/design/{id}")
    public DesignUnit getDesignUnitById(@PathVariable("id") int id) {
        return designUnitRepository.findById(id).get();
    }

    @PostMapping("/design")
    public ResponseEntity<DesignUnit> createDesignUnit(@RequestBody DesignUnit pDesignUnit) {
        try {
            return new ResponseEntity<>(designUnitRepository.save(pDesignUnit), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/design/{id}")
    public ResponseEntity<DesignUnit> updateDesign(@RequestBody DesignUnit pDesignUnit, @PathVariable("id") int id) {
        try {
            Optional<DesignUnit> designData = designUnitRepository.findById(id);
            if (designData.isPresent()) {
                designData.get().setAddress(pDesignUnit.getAddress());
                designData.get().setEmail(pDesignUnit.getEmail());
                designData.get().setFax(pDesignUnit.getFax());
                designData.get().setName(pDesignUnit.getName());
                designData.get().setNote(pDesignUnit.getNote());
                designData.get().setPhone(pDesignUnit.getPhone());
                designData.get().setPhone2(pDesignUnit.getPhone2());
                designData.get().setProjects(pDesignUnit.getProjects());
                designData.get().setDescription(pDesignUnit.getDescription());
                designData.get().setWebsite(pDesignUnit.getWebsite());
                return new ResponseEntity<>(designUnitRepository.save(designData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/design/{id}")
    public ResponseEntity<Customers> deleteCustomer(@PathVariable("id") int id) {
        designUnitRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
