package com.devcamp.projectestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.projectestate.entity.District;
import com.devcamp.projectestate.repository.DistrictRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DistrictController {
    @Autowired
    DistrictRepository districtRepository;

    @GetMapping("/district")
    public List<District> getAllDistrict() {
        return districtRepository.findAll();
    }

    @GetMapping("/district/{id}")
    public District getDistrictById(@PathVariable("id") int id) {
        return districtRepository.findById(id).get();
    }

}
