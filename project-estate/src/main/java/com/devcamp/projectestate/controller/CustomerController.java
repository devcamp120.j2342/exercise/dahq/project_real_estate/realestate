package com.devcamp.projectestate.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.projectestate.entity.Customers;
import com.devcamp.projectestate.repository.CustomerRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/customer")
    public List<Customers> getAllCustomers() {
        return customerRepository.findAll();

    }

    @GetMapping("/customer/{id}")
    public Customers getCustomer(@PathVariable("id") long id) {
        return customerRepository.findById(id).get();
    }

    @PostMapping("/customer")
    public ResponseEntity<Customers> createCustomer(@RequestBody Customers pCustomers) {
        try {
            Customers customerData = new Customers();
            customerData.setAddress(pCustomers.getAddress());
            customerData.setContactName(pCustomers.getContactName());
            customerData.setContactTitle(pCustomers.getContactTitle());
            customerData.setCreateBy(pCustomers.getCreateBy());
            customerData.setCreateDate(new Date());
            customerData.setEmail(pCustomers.getEmail());
            customerData.setMobile(pCustomers.getMobile());
            customerData.setNote(pCustomers.getNote());
            customerData.setUpdateBy(pCustomers.getUpdateBy());
            customerData.setUpdateDate(new Date());

            return new ResponseEntity<>(customerRepository.save(pCustomers), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<Customers> updateCustomer(@RequestBody Customers pCustomers, @PathVariable("id") long id) {
        try {
            Optional<Customers> customerData = customerRepository.findById(id);
            if (customerData.isPresent()) {
                customerData.get().setAddress(pCustomers.getAddress());
                customerData.get().setContactName(pCustomers.getContactName());
                customerData.get().setContactTitle(pCustomers.getContactTitle());
                customerData.get().setCreateBy(pCustomers.getCreateBy());
                customerData.get().setCreateDate(new Date());
                customerData.get().setEmail(pCustomers.getEmail());
                customerData.get().setMobile(pCustomers.getMobile());
                customerData.get().setNote(pCustomers.getNote());
                customerData.get().setUpdateBy(pCustomers.getUpdateBy());
                customerData.get().setUpdateDate(new Date());
                return new ResponseEntity<>(customerRepository.save(customerData.get()), HttpStatus.OK);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity<Customers> deleteCustomer(@PathVariable("id") long id) {
        customerRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
