package com.devcamp.projectestate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.projectestate.entity.Employees;
import com.devcamp.projectestate.repository.EmployeeRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("/employee")
    public List<Employees> getAllEmlpoyee() {
        return employeeRepository.findAll();
    }

    @GetMapping("/employee/{id}")
    public Employees getEmployeeById(@PathVariable("id") int id) {
        return employeeRepository.findById(id).get();
    }

    @PostMapping("/employee")
    public ResponseEntity<Employees> createEmployee(@RequestBody Employees pEmployees) {
        try {
            return new ResponseEntity<>(employeeRepository.save(pEmployees), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<Employees> updateEmlpoyee(@PathVariable("id") int id, @RequestBody Employees pEmployees) {
        try {
            Optional<Employees> employeeData = employeeRepository.findById(id);
            if (employeeData.isPresent()) {
                employeeData.get().setActivated(pEmployees.getActivated());
                employeeData.get().setBirthDate(pEmployees.getBirthDate());
                employeeData.get().setCity(pEmployees.getCity());
                employeeData.get().setCountry(pEmployees.getCountry());
                employeeData.get().setEmail(pEmployees.getEmail());
                employeeData.get().setExtension(pEmployees.getExtension());
                employeeData.get().setFirstName(pEmployees.getFirstName());
                employeeData.get().setHireDate(pEmployees.getHireDate());
                employeeData.get().setHomePhone(pEmployees.getHomePhone());
                employeeData.get().setLastName(pEmployees.getLastName());
                employeeData.get().setNotes(pEmployees.getNotes());
                employeeData.get().setPassword(pEmployees.getPassword());
                employeeData.get().setPhoto(pEmployees.getPhoto());
                employeeData.get().setPostalCode(pEmployees.getPostalCode());
                employeeData.get().setProfile(pEmployees.getProfile());
                employeeData.get().setRegion(pEmployees.getRegion());
                employeeData.get().setReportsTo(pEmployees.getReportsTo());
                employeeData.get().setTitle(pEmployees.getTitle());
                employeeData.get().setTitleOfCourtesy(pEmployees.getTitleOfCourtesy());
                employeeData.get().setUserLevel(pEmployees.getUserLevel());
                employeeData.get().setUserName(pEmployees.getUserName());
                return new ResponseEntity<>(employeeRepository.save(employeeData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/employee/{id}")
    public ResponseEntity<Employees> deleteEmployee(@PathVariable("id") int id) {
        employeeRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
