package com.devcamp.projectestate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.projectestate.entity.Investor;
import com.devcamp.projectestate.repository.InvestorRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class InvestorController {
    @Autowired
    InvestorRepository investorRepository;

    @GetMapping("/investor")
    public List<Investor> getAllInvestor() {
        return investorRepository.findAll();

    }

    @GetMapping("/investor/{id}")
    public Investor getInvestorById(@PathVariable("id") int id) {
        return investorRepository.findById(id).get();
    }

    @PostMapping("/investor")
    public ResponseEntity<Investor> createInvestor(@RequestBody Investor pInvestor) {
        try {
            return new ResponseEntity<>(investorRepository.save(pInvestor), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/investor/{id}")
    public ResponseEntity<Investor> updateInvestor(@PathVariable("id") int id, @RequestBody Investor pInvestor) {
        try {
            Optional<Investor> investorData = investorRepository.findById(id);
            if (investorData.isPresent()) {
                investorData.get().setAddress(pInvestor.getAddress());
                investorData.get().setDescription(pInvestor.getDescription());
                investorData.get().setEmail(pInvestor.getEmail());
                investorData.get().setFax(pInvestor.getFax());
                investorData.get().setName(pInvestor.getName());
                investorData.get().setNote(pInvestor.getNote());
                investorData.get().setPhone(pInvestor.getPhone());
                investorData.get().setPhone2(pInvestor.getPhone2());
                investorData.get().setProjects(pInvestor.getProjects());
                investorData.get().setWebsite(pInvestor.getWebsite());
                return new ResponseEntity<>(investorRepository.save(investorData.get()), HttpStatus.OK);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/investor/{id}")
    public ResponseEntity<Investor> deleteteInvestor(@PathVariable("id") int id) {
        investorRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
