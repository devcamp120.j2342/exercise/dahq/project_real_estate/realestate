package com.devcamp.projectestate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.projectestate.entity.Utilities;
import com.devcamp.projectestate.repository.UtilitesRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class UtilitiesController {
    @Autowired
    UtilitesRepository utilitesRepository;

    @GetMapping("/utilities")
    public List<Utilities> getAllUtilities() {
        return utilitesRepository.findAll();
    }

    @GetMapping("/utilities/{id}")
    public Utilities getUtilitiesById(@PathVariable("id") int id) {
        return utilitesRepository.findById(id).get();
    }

    @PostMapping("/utilities")
    public ResponseEntity<Utilities> createUtilities(@RequestBody Utilities pUtilities) {
        try {
            return new ResponseEntity<>(utilitesRepository.save(pUtilities), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/utilities/{id}")
    public ResponseEntity<Utilities> updateUtilities(@PathVariable("id") int id, @RequestBody Utilities pUtilities) {
        try {
            Optional<Utilities> utilitiesData = utilitesRepository.findById(id);
            if (utilitiesData.isPresent()) {
                utilitiesData.get().setName(pUtilities.getName());
                utilitiesData.get().setDescription(pUtilities.getDescription());
                utilitiesData.get().setPhoto(pUtilities.getPhoto());
                return new ResponseEntity<>(utilitesRepository.save(utilitiesData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/utilities/{id}")
    public ResponseEntity<Utilities> deleteUtilities(@PathVariable("id") int id) {
        utilitesRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
