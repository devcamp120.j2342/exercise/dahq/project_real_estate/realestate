package com.devcamp.projectestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.projectestate.entity.Ward;
import com.devcamp.projectestate.repository.WardRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class WardController {
    @Autowired
    WardRepository wardRepository;

    @GetMapping("/ward")
    public List<Ward> getAllWard() {
        return wardRepository.findAll();
    }

    @GetMapping("/ward/{id}")
    public Ward getWardById(@PathVariable("id") int id) {
        return wardRepository.findById(id).get();
    }

}
